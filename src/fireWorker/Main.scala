package fireWorker

import game.hanabi.Hanabi
import game.hanabi.HanabiDeck

object Main extends App {
  
  val scores = List.range(1, 1000).map(_ => {
    new Hanabi(HanabiDeck.shuffledDeck(), 4).sequenceOfPlay.last.score
  })
  
  val sum = scores.sum
  val mean = sum / scores.size
  val devs = scores.map(score => (score - mean) * (score - mean))
  val stddev = Math.sqrt(devs.reduce(_ + _) / devs.size)
  
  println("Mean: " + mean)
  println("Median: " + scores.sortWith(_ < _)(scores.size / 2))
  println("Max: " + scores.max)
  println("Min: " + scores.min)
  println("Standard deviation: " + stddev)
}