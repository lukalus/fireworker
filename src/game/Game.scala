package game

abstract class Game {
  type StateOfPlay
  type Move <: StateOfPlay => StateOfPlay
  
  val initialState: StateOfPlay
  
  val sequenceOfPlay: Stream[StateOfPlay] = {
    def followingStates(currentState: StateOfPlay): Stream[StateOfPlay] = {
      if (stateIsFinal(currentState)) currentState #:: Stream()
      else nextMove(currentState) match {
        case None => currentState #:: Stream()
        case Some(move) => currentState #:: followingStates(move(currentState))
      }
    }
    followingStates(initialState)
  }
  
  def stateIsFinal(currentState: StateOfPlay): Boolean
  
  def valueOfMove(move: Move, currentState: StateOfPlay): Option[Int]
  
  def reasonableMoves(currentState: StateOfPlay): List[Move]
  
  def nextMove(currentState: StateOfPlay): Option[Move] = {
    val movesWithValues = for {
      move <- reasonableMoves(currentState)
      value <- valueOfMove(move, currentState)
    } yield (move, value)
    if (movesWithValues.isEmpty) None
    else Some(movesWithValues.maxBy(_._2)._1) // TODO handle ties with randomization? split lists into trees?
  }
}