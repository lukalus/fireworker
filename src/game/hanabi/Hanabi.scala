package game.hanabi

import scala.util.Random

import game.Game

object Color extends Enumeration {
  type Color = Value
  val Blue, Red, Green, White, Yellow = Value
  val allColors = Blue :: Red :: Green :: White :: Yellow :: Nil
}
import Color._

object MoveType extends Enumeration {
  type MoveType = Value
  val Play, Discard, Hint = Value
}
import MoveType._

class Hanabi(val initialDeck: List[Card], val numPlayers: Int) extends Game {
  type StateOfPlay = HanabiState
  type Move = HanabiMove
  
  lazy val initialState = {
    val predealingPlayers = for {
      n <- List.range(1, numPlayers + 1)
    } yield new Player(new Hand(Nil, Map()), Nil, n)
    val predealingPlayedCards = for (color <- allColors) yield (color, Nil)
    val predealingState = new StateOfPlay(Map() ++ predealingPlayedCards, initialDeck, predealingPlayers)
    
    def deal(currentState: StateOfPlay): StateOfPlay = {
      if (currentState.players.forall(_.hand.cards.size == 4)) currentState
      else {
        val dealtPlayer = currentState.currentPlayer.withNewCard(currentState.remainingCards.head, 0)
        deal(currentState.withRemainingCards(currentState.remainingCards.tail).withPlayers(currentState.players.tail :+ dealtPlayer))
      }
    }
    
    deal(predealingState)
  }
  
  def stateIsFinal(state: StateOfPlay) = {
    state.movesSinceNoCardsRemaining == numPlayers || state.strikesRemaining == 0
  }
  
  def valueOfMove(move: Move, currentState: StateOfPlay): Option[Int] = {
    Some(1) // TODO stub
  }
  
  def reasonableMoves(currentState: StateOfPlay): List[Move] = {
    // TODO determine moves only based on subjective HanabiState (only what's accessible to current player)
    val currentCards = currentState.currentPlayer.hand.cards
    lazy val safePlays = for {
      card <- currentCards
      if knownSafeToPlay(currentState, card)
    } yield play(card)
    lazy val semiSafePlays = for {
      card <- currentCards
      if semiSafeToPlay(currentState, card, currentState.currentPlayer)
    } yield play(card)
    lazy val safeDiscards = for {
      card <- currentCards
      if knownSafeToDiscard(currentState, card)
    } yield discard(card)
    lazy val unsafeDiscards = {
      val cardToDiscard = currentState.currentPlayer.hand.cards.head
      new HanabiMove(Discard, (state: StateOfPlay) => state.withCardHavingBeenDiscarded(cardToDiscard), Some(cardToDiscard)) :: Nil
    }
    
    def numberDatumForPlayerAboutCard(player: Player, card: Card): Datum = {
      new Datum(player.id, player.hand.idsOfCardsWithNumber(card.number), player.hand.idsOfCardsWithoutNumber(card.number), None, Some(card.number), currentState.turn)
    }
    def colorDatumForPlayerAboutCard(player: Player, card: Card): Datum = {
      new Datum(player.id, player.hand.idsOfCardsWithColor(card.color), player.hand.idsOfCardsWithoutColor(card.color), Some(card.color), None, currentState.turn)
    }
    lazy val playabilityOfHintableCards = for {
      player <- currentState.players.tail
      card <- player.hand.cards
    } yield (player, card, currentState.distanceToCardBeingPlayable(card))
    def safeNumberHintsForCardsWithDistanceProperty(distanceHasProperty: (Int => Boolean)): List[Move] = {
      for {
        (player, card, distance) <- playabilityOfHintableCards
        if distanceHasProperty(distance)
        val datum = numberDatumForPlayerAboutCard(player, card)
        if datumHasNewInfo(currentState, datum) && datumHasNewSafeInfo(currentState, datum)
      } yield hint(datum)
    }
    def numberHintsForCardsWithDistanceProperty(distanceHasProperty: (Int => Boolean)): List[Move] = {
      for {
        (player, card, distance) <- playabilityOfHintableCards
        if distanceHasProperty(distance)
        val datum = numberDatumForPlayerAboutCard(player, card)
        if datumHasNewInfo(currentState, datum)
      } yield hint(datum)
    }
    def colorHintsForCardsWithDistanceProperty(distanceHasProperty: (Int => Boolean)): List[Move] = {
      for {
        (player, card, distance) <- playabilityOfHintableCards
        if distanceHasProperty(distance)
        val datum = colorDatumForPlayerAboutCard(player, card)
        if datumHasNewInfo(currentState, datum)
      } yield hint(datum)
    }
    def hintsForUnsafeSemiPlayableCardsWithDistanceProperty(distanceHasProperty: (Int => Boolean)): List[Move] = {
      for {
        (player, card, distance) <- playabilityOfHintableCards
        (_, knownNumber, _) = player.knownStatesForCard(card)
        if knownNumber.isDefined
        if semiSafeToPlay(currentState, card, player)
        if distanceHasProperty(distance)
        val datum = colorDatumForPlayerAboutCard(player, card)
        if datumHasNewInfo(currentState, datum)
      } yield hint(datum)
    }
    
    lazy val hintsForUnsafeSemiPlayableCards = hintsForUnsafeSemiPlayableCardsWithDistanceProperty(_ != 0)
    lazy val safeNumberHintsForPlayableCards = safeNumberHintsForCardsWithDistanceProperty(_ == 0)
    lazy val numberHintsForPlayableCards = numberHintsForCardsWithDistanceProperty(_ == 0)
    lazy val colorHintsForPlayableCards = colorHintsForCardsWithDistanceProperty(_ == 0)
    lazy val hintsForEventuallyPlayableCards = numberHintsForCardsWithDistanceProperty(_ > 0) ++ colorHintsForCardsWithDistanceProperty(_ > 0)
    
    if (false) unsafeDiscards // not really
    else if (currentState.hintTokensRemaining > 0 && !hintsForUnsafeSemiPlayableCards.isEmpty && currentState.strikesRemaining > 1) hintsForUnsafeSemiPlayableCards
    else if (!safePlays.isEmpty) safePlays
    else if (!semiSafePlays.isEmpty && currentState.strikesRemaining > 1) semiSafePlays
    else if (currentState.hintTokensRemaining == 0) {
      if (!safeDiscards.isEmpty) safeDiscards
      else unsafeDiscards
    }
    else {
      if (!safeNumberHintsForPlayableCards.isEmpty) safeNumberHintsForPlayableCards
      else if (!numberHintsForPlayableCards.isEmpty) numberHintsForPlayableCards
      else if (!colorHintsForPlayableCards.isEmpty) colorHintsForPlayableCards
      else if (currentState.hintTokensRemaining > 2 && !hintsForEventuallyPlayableCards.isEmpty) hintsForEventuallyPlayableCards
      else if (!safeDiscards.isEmpty) safeDiscards
      else unsafeDiscards
    }
  }
  
  def knownSafeToDiscard(currentState: StateOfPlay, card: Card): Boolean = {
    val (knownColor, knownNumber, knownExactCard) = currentState.currentPlayer.knownStatesForCard(card)
    (knownColor.exists(currentState.colorFullyPlayed)
      || knownNumber.exists(_ <= currentState.largestNumberFullyPlayed)
      || knownExactCard.exists(currentState.cardHasBeenPlayed))
  }
  
  def discard(card: Card): Move = {
    new HanabiMove(Discard, (state: StateOfPlay) => state.withCardHavingBeenDiscarded(card), Some(card))
  }
  
  def knownSafeToPlay(currentState: StateOfPlay, card: Card): Boolean = {
    val (_, knownNumber, knownExactCard) = currentState.currentPlayer.knownStatesForCard(card)
    (knownNumber.exists(currentState.numberDefinitelyPlayable) || knownExactCard.exists(currentState.cardIsPlayable))
  }
  
  def semiSafeToPlay(currentState: StateOfPlay, card: Card, player: Player): Boolean = {
    val (_, knownNumber, _) = player.knownStatesForCard(card)
    if (knownNumber.isEmpty) false
    else currentState.numberSemiPlayable(knownNumber.get)
  }
  
  def play(card: Card): Move = {
    new HanabiMove(Play, (state: StateOfPlay) => state.withCardHavingBeenPlayed(card), Some(card))
  }
  
  def cardNumberHasAlreadyBeenClued(currentState: StateOfPlay, card: Card): Boolean = {
    val otherCards = for {
      player <- currentState.players.tail
      oCard <- player.hand.cards
      val (_, knownNumber, _) = player.knownStatesForCard(oCard)
      if (card.color == oCard.color && card.number == oCard.number && knownNumber.isDefined)
    } yield oCard
    !otherCards.isEmpty
  }
  
  def datumHasNewInfo(currentState: StateOfPlay, datum: Datum): Boolean = {
    val player = currentState.playerWithId(datum.playerId)
    val relevantCards = for {
      id <- datum.cardIds
      card <- player.hand.cardWithId(id)
    } yield card
    def hasNewInfoForCard(card: Card): Boolean = {
      if (!datum.cardIds.contains(card.id)) false
      else {
        val (knownColor, knownNumber, _) = player.knownStatesForCard(card)
        (datum.color, datum.number) match {
          case (Some(newColor), None) => !knownColor.contains(newColor)
          case (None, Some(newNumber)) => !knownNumber.contains(newNumber)
          case _ => false // impossible
        }
      }
    }
    relevantCards.exists(hasNewInfoForCard)
  }
  
  def datumHasNewSafeInfo(currentState: StateOfPlay, datum: Datum): Boolean = {
    val player = currentState.playerWithId(datum.playerId)
    val relevantCards = for {
      id <- datum.cardIds
      card <- player.hand.cardWithId(id)
    } yield card
    def hasSafeInfoForCard(card: Card): Boolean = {
      if (!datum.cardIds.contains(card.id)) false
      else {
        val (knownColor, knownNumber, _) = player.knownStatesForCard(card)
        (datum.color, datum.number) match {
          case (Some(newColor), None) => true
          case (None, Some(newNumber)) => !cardNumberHasAlreadyBeenClued(currentState, card) && currentState.cardIsPlayable(card) || knownColor.isDefined || knownNumber.isDefined
          case _ => false // impossible
        }
      }
    }
    relevantCards.forall(hasSafeInfoForCard)
  }
    
  def hint(datum: Datum): Move = {
    new HanabiMove(Hint, (state: StateOfPlay) => state.withHintHavingBeenGiven(datum))
  }
}

class Card(val color: Color, val number: Int, val id: Int) { // TODO subtype/param to allow representing cards a player can't see in a subjective HanabiState
  require(number >= 1 && number <= 5)
  
  override def toString(): String = {color.toString().charAt(0) + number.toString()}
}

object HanabiDeck {
  val orderedDeck = {
    val numbers = 1 :: 1 :: 1 :: 2 :: 2 :: 3 :: 3 :: 4 :: 4 :: 5 :: Nil
    val combos = for {
      color <- allColors
      number <- numbers
    } yield (color, number)
    (combos zip List.range(0, 49)) map {case ((col, num), id) => new Card(col, num, id)}
  }
  
  def shuffledDeck(): List[Card] = {
    Random.shuffle(orderedDeck)
  }
}

class HanabiMove(val moveType: MoveType, val application: (HanabiState => HanabiState), val card: Option[Card] = None) extends Function1[HanabiState, HanabiState] {
  def apply(state: HanabiState): HanabiState = application(state)
}