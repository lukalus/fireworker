package game.hanabi

import Color._

class Player(
    val hand: Hand,
    val knownData: List[Datum],
//    val inferredData: List[Datum], // TODO
    val id: Int) {
  def withNewHand(newHand: Hand): Player = {
    new Player(newHand, knownData, id)
  }
  
  def withNewCard(newCard: Card, turn: Int): Player = {
    withNewHand(hand.withCard(newCard, turn))
  }
  
  def withoutCard(card: Card): Player = {
    withNewHand(hand.withoutCard(card))
  }
  
  def withNewDatum(newDatum: Datum): Player = {
    new Player(hand, newDatum :: knownData, id)
  }
  
  def withNewData(newData: List[Datum]): Player = {
    newData match {
      case Nil => this
      case datum :: data => withNewDatum(datum).withNewData(data)
    }
  }
  
  def withInferredDataGivenVisibleCards(visibleCards: Set[Card]): Player = {
    val cardsNotSeen = HanabiDeck.orderedDeck.filter(!visibleCards.contains(_))
    val inferredData = for {
      card <- hand.cards
      datum <- inferredDataForCard(card, cardsNotSeen)
    } yield datum
    withNewData(inferredData)
  }
  
  def knownDataForCard(card: Card): List[Datum] = {
    for {
      datum <- knownData
      if datum.cardIds.contains(card.id)
    } yield datum
  }
  
  def dataKnownNotToApplyToCard(card: Card): List[Datum] = {
    for {
      datum <- knownData
      if datum.cardIdsNotHinted.contains(card.id)
    } yield datum
  }
  
  def inferredDataForCard(card: Card, cardsNotSeen: List[Card]): List[Datum] = {
    val (knownColor, knownNumber, knownExactCard) = knownStatesForCard(card)
    if (!knownExactCard.isEmpty) Nil // don't bother inferring anything if the card is already known
    else {
      val dataKnownNotToApply = dataKnownNotToApplyToCard(card)
      lazy val colorsKnownNotToInhereInCard = (for (datum <- dataKnownNotToApply; color <- datum.color) yield color).toSet
      lazy val numbersKnownNotToInhereInCard = (for (datum <- dataKnownNotToApply; number <- datum.number) yield number).toSet
      def cardMightMatchColor(cardNotSeen: Card): Boolean = {
        if (knownColor.isEmpty) !colorsKnownNotToInhereInCard.contains(cardNotSeen.color)
        else knownColor.get == cardNotSeen.color
      }
      def cardMightMatchNumber(cardNotSeen: Card): Boolean = {
        if (knownNumber.isEmpty) !numbersKnownNotToInhereInCard.contains(cardNotSeen.number)
        else knownNumber.get == cardNotSeen.number
      }
      val candidatesForGivenCard = cardsNotSeen.filter((cardNotSeen: Card) => cardMightMatchColor(cardNotSeen) && cardMightMatchNumber(cardNotSeen))
      lazy val candidateColors = candidatesForGivenCard.map(_.color).toSet
      lazy val candidateNumbers = candidatesForGivenCard.map(_.number).toSet
      val colorDatum = if (knownColor.isEmpty && candidateColors.size == 1) {
        Some(new Datum(id, Set(card.id), Set(), Some(candidateColors.head), None, 0))
      } else None
      val numberDatum = if (knownNumber.isEmpty && candidateNumbers.size == 1) {
        Some(new Datum(id, Set(card.id), Set(), None, Some(candidateNumbers.head), 0))
      } else None
      List(colorDatum, numberDatum).flatten
    }
  }
  
  def knownStatesForCard(card: Card): (Option[Color], Option[Int], Option[Card]) = {
    val relevantData = knownDataForCard(card)
    val knownColor = for (datum <- relevantData; color <- datum.color) yield color
    val knownNumber = for (datum <- relevantData; number <- datum.number) yield number
    val knownExactCard = for (color <- knownColor; number <- knownNumber) yield new Card(color, number, 0)
    (knownColor.headOption, knownNumber.headOption, knownExactCard.headOption)
  }
  
  override def toString(): String = {
    def cardWithData(card: Card): String = {
      val (knownColor, knownNumber, _) = knownStatesForCard(card)
      val colorString = if (knownColor.isEmpty) card.color.toString().charAt(0) else "!" + card.color.toString().charAt(0)
      val numberString = if (knownNumber.isEmpty) card.number.toString() else "!" + card.number
      colorString + numberString
    }
    "(id " + id + "; cards " + hand.cards.map(cardWithData) + ")"
  }
}

class Hand(val cards: List[Card],
    val cardWithIdDrawnOnTurn: Map[Int, Int]) {
  require(cards.size <= 4)
  
  def withCard(card: Card, turn: Int): Hand = {
    new Hand(card :: cards, cardWithIdDrawnOnTurn + ((card.id, turn)))
  }
  
  def withoutCard(card: Card): Hand = {
    new Hand(cards.filterNot(_.id == card.id), cardWithIdDrawnOnTurn)
  }
  
  def cardWithId(id: Int): Option[Card] = {
    cards.filter(_.id == id).headOption
  }
  
  def idsOfCards(cards: List[Card]): Set[Int] = {
    cards.map(_.id).toSet
  }
  
  def idsOfCardsWithColor(color: Color): Set[Int] = {
    idsOfCards(cards.filter(_.color == color))
  }
  
  def idsOfCardsWithoutColor(color: Color): Set[Int] = {
    idsOfCards(cards.filter(_.color != color))
  }
  
  def idsOfCardsWithNumber(number: Int): Set[Int] = {
    idsOfCards(cards.filter(_.number == number))
  }
  
  def idsOfCardsWithoutNumber(number: Int): Set[Int] = {
    idsOfCards(cards.filter(_.number != number))
  }
}

class Datum(
    val playerId: Int,
    val cardIds: Set[Int],
    val cardIdsNotHinted: Set[Int],
    val color: Option[Color],
    val number: Option[Int],
    val givenOnTurn: Int) {
  require((color == None || number == None) && !(color == None && number == None))
}