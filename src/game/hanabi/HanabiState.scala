package game.hanabi

import Color._

class HanabiState(
    val playedCards: Map[Color, List[Card]],
    val remainingCards: List[Card],
    val players: List[Player],
    val movesSinceNoCardsRemaining: Int = 0,
    val hintTokensRemaining: Int = 8,
    val strikesRemaining: Int = 3,
    val discardedCards: List[Card] = Nil,
    val turn: Int = 0) {
  val totalHintTokens = 8
  require(hintTokensRemaining >= 0 && hintTokensRemaining <= totalHintTokens)
  
  def withRemainingCards(remainingCards: List[Card]): HanabiState = {
    new HanabiState(playedCards, remainingCards, players, movesSinceNoCardsRemaining, hintTokensRemaining, strikesRemaining, discardedCards, turn)
  }
  
  def withPlayers(players: List[Player]): HanabiState = {
    new HanabiState(playedCards, remainingCards, players, movesSinceNoCardsRemaining, hintTokensRemaining, strikesRemaining, discardedCards, turn)
  }
  
  def withCurrentPlayerHavingDrawn: HanabiState = {
    remainingCards match {
      case Nil => this
      case topCard :: _ => withPlayers(currentPlayer.withNewCard(topCard, turn) :: players.tail).withRemainingCards(remainingCards.tail)
    }
  }
  
  def withPlayHavingPassed: HanabiState = {
    val movesSinceNoCardsRemaining = if (remainingCards.isEmpty) this.movesSinceNoCardsRemaining + 1 else this.movesSinceNoCardsRemaining
    val withMovesSinceUpdated = new HanabiState(playedCards, remainingCards, players, movesSinceNoCardsRemaining, hintTokensRemaining, strikesRemaining, discardedCards, turn + 1)
    withMovesSinceUpdated.withPlayers(players.tail :+ players.head)//.withPlayersHavingInferredNewData
  }
  
  def withNumHintTokens(numHintTokens: Int): HanabiState = {
    val hintTokensRemaining = math.max(math.min(numHintTokens, totalHintTokens), 0)
    new HanabiState(playedCards, remainingCards, players, movesSinceNoCardsRemaining, hintTokensRemaining, strikesRemaining, discardedCards, turn)
  }
  
  def withCardOutOfPlay(card: Card): HanabiState = {
    new HanabiState(playedCards, remainingCards, players, movesSinceNoCardsRemaining, hintTokensRemaining, strikesRemaining, card :: discardedCards, turn)
  }
  
  def withCardHavingBeenDiscarded(card: Card): HanabiState = {
    withPlayers(currentPlayer.withoutCard(card) :: players.tail).withCardOutOfPlay(card).withNumHintTokens(hintTokensRemaining + 1).withCurrentPlayerHavingDrawn.withPlayHavingPassed
  }
  
  def withPlayedCard(card: Card): HanabiState = {
    val playedCards = this.playedCards + ((card.color, card :: this.playedCards(card.color)))
    new HanabiState(playedCards, remainingCards, players, movesSinceNoCardsRemaining, hintTokensRemaining, strikesRemaining, discardedCards, turn)
  }
  
  def withStrikesRemaining(strikesRemaining: Int): HanabiState = {
    new HanabiState(playedCards, remainingCards, players, movesSinceNoCardsRemaining, hintTokensRemaining, strikesRemaining, discardedCards, turn)
  }
  
  def withCardHavingBeenPlayed(card: Card): HanabiState = {
    val withCardDown = {
      if (cardIsPlayable(card)) withPlayedCard(card)
      else withCardOutOfPlay(card).withStrikesRemaining(strikesRemaining - 1)
    }
    withCardDown.withPlayers(currentPlayer.withoutCard(card) :: players.tail).withCurrentPlayerHavingDrawn.withPlayHavingPassed
  }
  
  def withHintHavingBeenGiven(datum: Datum): HanabiState = {
    def playerWithHint = (player: Player) => if (player.id == datum.playerId) player.withNewDatum(datum) else player
    withPlayers(players.map(playerWithHint)).withNumHintTokens(hintTokensRemaining - 1).withPlayHavingPassed
  }
  
  def withPlayersHavingInferredNewData(): HanabiState = {
    withPlayers(players.map((player: Player) => {
      player.withInferredDataGivenVisibleCards(cardsVisibleToPlayer(player))
    }))
  }
  
//  def asKnownToPlayer(player: Player): HanabiState // TODO
  
  lazy val currentPlayer: Player = {players.head}
  
  def cardHasBeenPlayed(card: Card): Boolean = {
    distanceToCardBeingPlayable(card) < 0
  }
  
  def cardIsPlayable(card: Card): Boolean = {
    distanceToCardBeingPlayable(card) == 0
  }
  
  def distanceToCardBeingPlayable(card: Card): Int = {
    card.number - playedCards(card.color).length - 1
  }
  
  def largestNumberPlayedForColor(color: Color): Int = {
    playedCards(color).size
  }
  
  def colorFullyPlayed(color: Color): Boolean = {
    (largestNumberPlayedForColor(color) == 5)
  }
  
  lazy val largestNumberFullyPlayed: Int = {
    val numsPlayed = for (number <- List.range(1, 5) if (cardsWithNumberAlreadyPlayed(number) == 5)) yield number
    (0 :: numsPlayed).max
  }
  
  def cardsWithNumberAlreadyPlayed(number: Int): Int = {
    val playedCardsWithNumber = for {
      (color, cards) <- playedCards
      card <- cards
      if (card.number == number)
    } yield card
    playedCardsWithNumber.size
  }
  
  def numberDefinitelyPlayable(number: Int): Boolean = {
    playedCards.values.forall(_.size + 1 == number)
  }
  
  def numberSemiPlayable(number: Int): Boolean = {
    playedCards.values.exists(_.size + 1 == number)
  }
  
  def playerWithId(id: Int): Player = {
    players.filter(_.id == id).head
  }
  
  def cardsVisibleToPlayer(player: Player): Set[Card] = {
    discardedCards.toSet ++ (playedCards.values.flatten) ++ (players.filter(_.id != player.id).map(_.hand.cards).flatten)
  }
  
  lazy val score = {
    if (strikesRemaining == 0) 0
    else playedCards.values.flatten.size
  }
  
  override def toString(): String = {
    "Played Cards: " + score + "; " + playedCards + "\n" +
    "Players: " + players + "\n" +
    "Strikes: " + strikesRemaining + ", Hints: " + hintTokensRemaining + ", Cards in deck: " + remainingCards.size + ", Cards out of play: " + discardedCards.size + "\n"
  }
}